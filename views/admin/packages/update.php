<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
 
    </ul>
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="test">
        <input type="hidden" name="id" value="<?php echo $model->package->id; ?>" />
        <input name="token" type="hidden" value="<?php echo get_token();?>" />
        <div class="row">
            <div class="col-md-24">
                <div class="box">
                    <h4>General</h4>
                    <div class="form-group">
                        <label>Name of package</label>
                        <?php echo $model->form->editorFor("name"); ?>
                    </div>
                     <div class="form-group">
                        <label>Number of session</label>
                        <?php echo $model->form->editorFor("quantity"); ?>
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        <?php echo $model->form->editorFor("price"); ?>
                    </div>
                    
                      <div class="form-group" id="per" style="display:none;">
                        <label></label>
                        <input  id="per_session" type="text" value=""   disabled/>
                    </div>
                    <div class="form-group">
                        <label>Expiration</label>
                        <?php echo $model->form->editorFor("expiration"); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Description</label>
                        <?php echo $model->form->textAreaFor("description", ["class"=>"ckeditor"]); ?>
                    </div>

                </div>
            </div>
 
        </div>
      </div>
    
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function() {
        var price = $.trim($("input[name='price']").val());
        var quantity = $.trim($("input[name='quantity']").val());
            var total = price / quantity;
            if(total>0){
                 $("#per").show();
            $("#per_session").val("$" + (parseInt(total * 100)) / 100 + " (per session)");}else{$("#per").hide();}
       

        $("input[name='price'], input[name='quantity']").on('keyup', function(e) {

            var price = $.trim($("input[name='price']").val());
            var quantity = $.trim($("input[name='quantity']").val());
            var total = price / quantity;
             if(total>0){
                 $("#per").show();
                $("#per_session").val("$" + (parseInt(total * 100)) / 100 + " (per session)");
            }else{$("#per").hide();}
        });
    });
</script>