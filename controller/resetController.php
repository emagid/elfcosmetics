<?php
use Model\User;

/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 12/7/15
 * Time: 11:21 AM
 */

class resetController extends siteController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function password(array $params = [])
    {
        if(!isset($params['hash']) || !($user = User::getItem(null, ['where' => ['hash' => $params['hash']]]))){
            $n = new \Notification\ErrorHandler('Invalid reset password Link.');
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL);
        }

        $_SESSION['resetPasswordUser'] = serialize($user);

        parent::index();
    }

    public function password_post()
    {
        // if password not same
        if($_POST['password'] != $_POST['confirm-password']){
            $n = new \Notification\ErrorHandler("Password doesn't match");
            $_SESSION["notification"] = serialize($n);
            redirect($_SERVER['HTTP_REFERER']);
        }

        if(!($user = unserialize($_SESSION['resetPasswordUser']))){
            $n = new \Notification\ErrorHandler("Please try again");
            $_SESSION["notification"] = serialize($n);
            redirect($_SERVER['HTTP_REFERER']);
        }

        $hash = \Emagid\Core\Membership::hash($_POST['password']);
        $user->password = $hash['password'];
        $user->hash = $hash['salt'];
        $user->save();

        $n = new \Notification\MessageHandler('Password update successfully! Please login again.');
        $_SESSION["notification"] = serialize($n);
        redirect(SITE_URL);
    }


    /**
     * For forget password, we always return true for security reason
     */
    public function email_post()
    {
        $user = User::getItem(null, ['where' => ['email' => $_POST['email']]]);
        if(!$user){
            return;
        }

        // send reset password email
        $this->sendResetPasswordEmail($user);
    }

    private function sendResetPasswordEmail($user)
    {
        $resetLink = $user->getPasswordResetLink();
        $email = new Email\MailMaster();
        $email->setTo(['email' => $user->email, 'name' => $user->full_name(),  'type' => 'to'])->setMergeTags(['FORGET_PASSWORD_LINK'=>$resetLink])->setTemplate('forget-password')->send();
    }
}